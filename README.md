# Saz
A Python mini-module for creating "bağlama" songs ("bağlama" is a traditional Turkish instrument).
## Installation
In your project's folder:
```
git clone https://gitlab.com/berkaygunduz/saz
```
## Data Format
Saz works like adding sounds end to end and create a final sound. So it should be defined composition of a sound file and it's notes.
#### Coding Notes
Definition of a code is like `Code_of_Note Code_of_Value`. For example `11 2` means `semi-quaver do`.
Note: Space should be there for separating codes.
#### Coding Compositions
Compositions should be written in `.txt` files. For coding compositions, there is two format: vertical and horizontal. In vertical format, defined notes should be added with `\n`. In Horizontal format, defined notes should be added with `,`.  In both formats, be careful about adding `\n` or `,` end of composition. It will cause an error if there is something in the end.
Vertical format example:
```
0 8
15 4
8 4
```
Horizontal format example:
```
0 8,11 4,9 4
```
## Examples
Creating a song from "horizontal_example.txt" (which is horizontal formatted):
```python3
>>> import saz
>>> saz.makeSound("horizontal_example", "h")
```
