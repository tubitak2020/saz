#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""A Python mini-module for creating "bağlama" songs ("bağlama" is a
traditional Turkish instrument)
"""


# turn counts each usage of create command for naming new files
turn = 0


def create(arr):
    """Creates a sound file with adding .mp3 files end-to-end

    Parameters
    ----------
    arr : list
        A list that contains lists which specify a note
        note : list
            Each note contain a note name and a note value
            name : int
                0 :
                    rest
                1 :
                    mi (E) (high pitch)
                2  :
                    re (D)
                3 :
                    do (C)
                4 :
                    si (B)
                5 :
                    la (A)
                6 :
                    sol (G)
                7 :
                    fa (F)
                8 :
                    mi (E)
                9 :
                    re (D)
                10 :
                    do diesis (C sharp)
                11 :
                    do (C)
                12 :
                    si (B)
                13 :
                    si flat 2 (B flat 2)
                14 :
                    si flat (B flat)
                15 :
                    la (A)
                16 :
                    sol (G) (low pitch)
            value : int
                1 :
                    triple croche
                2 :
                    semi-quaver
                4 :
                    quaver
                8 :
                    crotchet
                16 :
                    semibrevis
                32 :
                    semibreve

    Example
    -------
        >>> import saz
        >>> saz.create([[15, 8], [15, 4], [15, 8], [14, 2], [15, 2], [16, 4]])
    """
    global turn
    try:
        flag = b""
        for i in arr:
            with open(f"{__file__[:-6]}sounds/{i[0]}/{i[1]}.mp3", "rb") as s:
                flag += s.read()
        with open(f"Sound{turn}.mp3", "wb") as final:
            final.write(flag)
        turn += 1
    except Exception as e:
        print(f"'{e}' error token while creating mp3 file!")


def convert(name, fileType):
    """Converts a txt file information to a 2D list

    Parameters
    ----------
    name : str
        Name of file that will be converted
    fileType : str
        Format of the txt file

    Returns
    -------
    list
        A 2D array which is for create function
        If an error accurs returns an empty list
    """
    try:
        with open(f"{name}.txt", "r") as file:
            if fileType == "v":
                flagData = file.read().split(sep="\n")
            elif fileType == "h":
                flagData = file.read().split(sep=",")
            else:
                print(f"'{fileType}' is not a valid type!")
                return []
            flagData = flagData[:-1]
            data = []
            for i in flagData:
                data.append([i.split()[0], i.split()[1]])
            return data
    except Exception as e:
        print(f"'{e}' error token while converting txt file to array!")
        return []


def makeSound(name, fileType):
    """Creates a soundfile

    Parameters
    ----------
    name : str
        Name of txt file that will be converted to a sound file
    fileType : str
        Format of the txt file
    """
    create(convert(name, fileType))


def makeSoundV(name):
    """Creates a soundfile which is vertical format

    Parameters
    ----------
    name : str
        Name of txt file that will be converted to a sound file
    """
    create(convert(name, "v"))


def makeSoundH(name):
    """Creates a soundfile which is horizontal format

    Parameters
    ----------
    name : str
        Name of txt file that will be converted to a sound file
    """
    create(convert(name, "h"))

